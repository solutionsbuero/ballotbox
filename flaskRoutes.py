# General stuff
import json
import time
import random
import base64

# Interaction OS modules
import subprocess
import threading

# http/Server modules
import requests
import gevent
import grequests # UNBEDINGT drinlassen, flaskWrapper.publish funktioniert nur so (wieso auch immer)
from gevent.queue import Queue
from gevent.pywsgi import WSGIServer
from flask import Flask, render_template, redirect, request, session, Response, url_for, current_app, make_response

# DIY modules
from serverSentEvent import ServerSentEvent
from interfaces import OSInterface, DBInterface
from player import AudioPlayer
from gpio import ButtonReader

class flaskWrapper:

	def __init__(self):

		# Zunächst mal Flask starten
		self.app = Flask(__name__)

		# Platz für WSGIServer reservieren, siehe self.run()
		self.server = None

		# Interfaces aus flaskInterfaces.py anhängen
		self.dbif = DBInterface()  # API für Datenbank
		self.osif = OSInterface()  # API für Betriebssystem

		# Audio Player Instanz
		self.player = AudioPlayer()

		# Playback Thread
		self.playback_thead = None

		# Button Reader Instanz
		self.button_reader = ButtonReader()
		self.button_reader.init_reading()

		# Config laden
		self.config = self.dbif.get_config()

		# Liste aller Abonnenten von SSE, wird von self.subscribe() gebraucht
		self.subscriptions = []

		# Zustandsvariabeln
		self.polls_open = False
		self.playback_running = False

		self.next_vote = self.dbif.get_random_tracks()
		self.up_next = None
		self.current_results = None

	'''
	Webinterface grundsätzlich servieren
	'''
	def root(self):
		return redirect('/index')

	def template_proxy(self):
		serial_data = None  # TODO: Welche Daten mitliefern? # Done: keine, Angular-Ansatz
		return render_template("skelett.html", data=self.config)

	def dyncss(self, filename):
		r = make_response(render_template(filename, data=self.config))
		r.headers['content-type'] = "text/css"
		return r

	# Assets servieren
	def asset_proxy(self, path):
		return self.app.send_static_file('assets/' + path)

	# Triviales Stück html, das alle SSE-Events auflistet
	def sse_monitor(self):
		return self.app.send_static_file('sse_monitor.html')

	# Noch trivialers Stück html, das die Anzahl Abonnenten auflistet
	def sse_view_subs(self):
		return "Currently %d subscriptions" % len(self.subscriptions)

	'''
	SSE-Funktionen
	Hier wird's etwas verworren. Quelle des ganzen Zeugs:
	http://flask.pocoo.org/snippets/116/
	'''
	# Abonnieren: Endpoint, um mit Javascript auf Event-Queue zugreifen zu können
	def subscribe(self):
		def gen():
			q = Queue()
			self.subscriptions.append(q)
			try:
				while True:
					result = q.get()
					ev = ServerSentEvent(str(result))
					yield ev.encode()
			except GeneratorExit:  # Or maybe use flask signals
				self.subscriptions.remove(q)
		return Response(gen(), mimetype="text/event-stream")

	# Diese Funktion braucht dict als Input, baut daraus eine Post-Request an req_publish()
	def publish(self, msg):
		request_url = 'http://%s:%d/req_publish/%s' % (self.config['flask_server'], self.config['flask_port'], msg)
		response = requests.get(request_url)
		print("published %s, received %s" % (msg, response.text))

	# Baut den von publish() erhaltenen dict in einen String um und sendet den als Queue an self.subscriptions
	def req_publish(self, msg):

		def notify(msg):
			for sub in self.subscriptions[:]:
				sub.put(msg)

		gevent.spawn(notify, msg=msg)
		return "OK"

	'''
	SSE-Debugging/Testing Zeug
	
	# Wrapper, um self.publish zu testen
	def publish_text(self):
		self.publish("yup")
		return "done"

	# Per URL-Endpoint timestamp publizieren, bloss damit mal was publiziert wurde
	def publish_time(self):
		def notify():
			msg = str(time.time())
			for sub in self.subscriptions[:]:
				sub.put(msg)
		gevent.spawn(notify)
		return "OK"

	# geloopte Verpackung, um publish & req_publish zu testen
	def threaded_req_publish(self):
		time.sleep(8)
		while True:
			print("Publishing Timestamp...")
			msg = {'time': str(time.time())}
			self.publish(msg)
			time.sleep(2)
	'''

	def start_playback(self):
		self.playback_running = True
		msg = {"action": "playback_started"}
		msg = json.dumps(msg)
		self.req_publish(msg)
		self.next_track()
		return "ok"

	def stop_playback(self):
		self.playback_running = False
		print("playback running: ", self.playback_running)
		print("polls open: ", self.polls_open)
		return "ok"

	def shutdown(self):
		subprocess.call("sudo shutdown -h now", shell=True)
		return "ok"

	def on_20_left(self):
		msg = {"action": "20left"}
		msg = json.dumps(msg)
		self.req_publish(msg)
		return "ok"

	def on_10_left(self):
		self.button_reader.stop_reading()
		self.polls_open = False
		self.current_results = self.dbif.sum_votes()
		msg = {"action": "show_results", "options": self.next_vote, "results": self.current_results}
		msg = json.dumps(msg)
		self.req_publish(msg)
		return "ok"

	def on_track_finished(self):
		if self.playback_running:
			self.next_track()
		else:
			msg = {"action": "playback_stopped"}
			msg = json.dumps(msg)
			self.req_publish(msg)
		return "ok"

	def cast_vote(self, choice):
		if self.polls_open:
			self.dbif.add_vote(choice)
			msg = {"action": "got_vote", "for": choice}
			msg = json.dumps(msg)
			self.req_publish(msg)
		return "ok"

	def next_track(self):
		if self.current_results == None:
			self.current_results = self.dbif.sum_votes()
		voting_results = self.current_results
		print("sum_votes: ", voting_results)
		next_path = self.next_vote[voting_results['win_index']]['path']
		msg = {"current_track": "%s - %s" % (
		self.next_vote[voting_results['win_index']]['artist'], self.next_vote[voting_results['win_index']]['title'])}
		print("next_path: ", next_path)
		self.player.start_playback(next_path)
		self.dbif.empty_box()
		self.next_vote = self.dbif.get_random_tracks()
		msg.update({"action": "next_vote", "value": self.next_vote})
		msg = json.dumps(msg)
		self.req_publish(msg)
		self.button_reader.start_reading()
		self.polls_open = True


	'''
	Endpoints: Alle Funktionen an URLs hängen
	'''
	def add_all_endpoints(self):

		# Webinterface servieren generell
		self.app.add_url_rule('/', 'root', self.root, methods=['GET', 'POST'])
		self.app.add_url_rule('/index', 'template_proxy', self.template_proxy, methods=['GET', 'POST'])
		self.app.add_url_rule('/assets/<path:path>', 'asset_proxy', self.asset_proxy, methods=['GET'])
		self.app.add_url_rule('/dyncss/<filename>', 'dyncss', self.dyncss, methods=['GET'])

		# SSE
		self.app.add_url_rule('/subscribe', 'subscribe', self.subscribe, methods=['GET', 'POST'])
		self.app.add_url_rule('/req_publish/<msg>', 'req_publish', self.req_publish, methods=['GET'])
		self.app.add_url_rule('/sse_monitor', 'sse_monitor', self.sse_monitor, methods=['GET'])
		self.app.add_url_rule('/sse_view_subs', 'sse_view_subs', self.sse_view_subs, methods=['GET'])

		# Internal API
		self.app.add_url_rule('/on_track_finished', 'on_track_finished', self.on_track_finished, methods=['GET', 'POST'])
		self.app.add_url_rule('/start_playback', 'start_playback', self.start_playback, methods=['GET'])
		self.app.add_url_rule('/stop_playback', 'stop_playback', self.stop_playback, methods=['GET'])
		self.app.add_url_rule('/shutdown', 'shutdown', self.shutdown, methods=['GET'])
		self.app.add_url_rule('/on_20_left', 'on_20_left', self.on_20_left, methods=['GET'])
		self.app.add_url_rule('/on_10_left', 'on_10_left', self.on_10_left, methods=['GET'])
		self.app.add_url_rule('/cast_vote/<choice>', 'cast_vote', self.cast_vote, methods=['GET'])

	'''
	Run like hell! Den ganzen Mist starten
	Flasks eingebauter Server kommt mit SSE nicht klar, deshalb wird serviert von WSGIServer
	'''
	def run(self):
		self.add_all_endpoints()
		self.server = WSGIServer(self.config['flask_port'], self.app)
		self.server.serve_forever()


if __name__ == "__main__":
	server = flaskWrapper()
	server.run()

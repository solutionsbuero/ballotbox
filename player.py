# import pygame
# import math
import time
import requests
import threading
from interfaces import DBInterface
from mutagen.mp3 import MP3
# from mutagen.id3 import ID3
import vlc


class AudioPlayer:

	def __init__(self):
		self.dbif = DBInterface()
		self.config = self.dbif.get_config()
		self.playback_thread = None

	'''
	def playback(self, path):
		track = MP3(path)
		track_tags = ID3(path) # Debugging
		track_length = track.info.length
		print(track_length, track_tags["TIT2"]) # Debugging
		pygame.mixer.pre_init(frequency=track.info.sample_rate)
		pygame.mixer.init()
		pygame.mixer.music.load(path) # Blockt offenbar nicht gut genug, Delay erhöht Zuverlässigkeit
		time.sleep(2)  # Ladezeit des Songs von der Länge abhängig machen?, Divisor eher zufällig, hat sich bewährt
		pygame.mixer.music.play()
		time.sleep(1)
		if not pygame.mixer.music.get_busy():
			pygame.mixer.music.play()
		while pygame.mixer.music.get_busy():
			remaining = int(math.ceil(track_length - (pygame.mixer.music.get_pos()/1000)))
			if remaining == 20:
				url = "http://%s:%d/on_20_left" % (self.config['flask_server'], self.config['flask_port'])
				requests.get(url)
				time.sleep(1.5)
			if remaining == 10:
				url = "http://%s:%d/on_10_left" % (self.config['flask_server'], self.config['flask_port'])
				requests.get(url)
				time.sleep(1.5)
		else:
			pygame.mixer.quit()
			url = "http://%s:%d/on_track_finished" % (self.config['flask_server'], self.config['flask_port'])
			print("done, requesting ", url)
			r = requests.get(url)
	'''
	def get_length(self, path):
		track = MP3(path)
		return track.info.length*1000


	def vlc_playback(self, path):
		instance = vlc.Instance('--aout=alsa')
		player = instance.media_player_new()
		media = instance.media_new(path)
		media.get_mrl()
		player.set_media(media)
		# length = player.get_length()
		length = self.get_length(path)
		# print(length)
		player.audio_set_volume(50)
		player.play()
		while True:
			current_pos = player.get_time()
			remaining = length - current_pos
			if 19200 < remaining < 20000: # 20s left
				url = "http://%s:%d/on_20_left" % (self.config['flask_server'], self.config['flask_port'])
				requests.get(url)
				time.sleep(1)
			elif 9200 < remaining < 10000: # 10s left
				url = "http://%s:%d/on_10_left" % (self.config['flask_server'], self.config['flask_port'])
				requests.get(url)
				time.sleep(1)
			elif remaining < 500: # playback almost done
				time.sleep(0.5)
				player.stop()
				break
		url = "http://%s:%d/on_track_finished" % (self.config['flask_server'], self.config['flask_port'])
		print("Playback done, requesting ", url)
		requests.get(url)

	def start_playback(self, path):
		self.playback_thread = threading.Thread(target=self.vlc_playback, args=(path,))
		self.playback_thread.start()


if __name__ == "__main__":
	ap = AudioPlayer()
	ap.vlc_playback("/home/dinu/Music/07 Venedig.mp3")

import requests
import RPi.GPIO as GPIO
import threading
import multiprocessing
from interfaces import DBInterface
import time

class ButtonReader:

	def __init__(self):
		self.dbif = DBInterface()
		self.config = self.dbif.get_config()
		GPIO.setmode(GPIO.BCM)
		self.in_pins = [6, 26, 13, 19]
		self.state_now = 0
		self.state_last = 0
		self.state_comp = 0
		self.do_reading = False
		self.reading_thread = threading.Thread(target=self.worker)
		self.gpio_setup()

	def gpio_setup(self):
		for i in self.in_pins:
			GPIO.setup(i, GPIO.IN, pull_up_down=GPIO.PUD_UP)

	def read_pins(self):
		self.state_now = 0
		loop_index = 0
		for i in self.in_pins:
			self.state_now += (not GPIO.input(i)) << loop_index
			loop_index += 1
		self.state_comp = self.state_now & (~self.state_last)
		self.state_last = self.state_now
		return self.state_comp

	def push_states(self):
		for i in range(0, len(self.in_pins)):
			if (self.state_comp & (1 << i)) > 0:
				print("computed state: ", self.state_comp)
				print("Got a vote for %d, casting..." % i)
				requests.get("http://%s:%d/cast_vote/%d" % (self.config['flask_server'], self.config['flask_port'], i))

	def worker(self):
		while True:
			if True:
				self.read_pins()
				self.push_states()

	def init_reading(self):
		self.reading_thread.start()

	def start_reading(self):
		self.do_reading = True

	def stop_reading(self):
		self.do_reading = False

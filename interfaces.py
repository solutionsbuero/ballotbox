import sqlite3
import subprocess
import random
import os
import eyed3

class DBInterface:

	def __init__(self):
		self.db = sqlite3.connect("ballots.db")
		self.db.row_factory = sqlite3.Row
		self.con = self.db.cursor()
		self.config = self.get_config()

	def get_config(self):
		config_dict = {}
		int_values = ['flask_port', 'n_choices']
		sql = "SELECT * FROM config"
		self.con.execute(sql)
		result = self.con.fetchall()
		for row in result:
			config_dict.update({row['key']: row['value']})
		for i in int_values:
			try:
				config_dict[i] = int(config_dict[i])
			except KeyError:
				pass
		return config_dict

	def scan_files(self):
		sql = "DELETE FROM audiotracks WHERE 1"
		self.con.execute(sql)
		walk_list = []
		for(dirpath, dirnames, filenames) in os.walk(self.config['music_dir']):
			for file in filenames:
				walk_list.append(dirpath + '/' + file)
		for mp3 in walk_list:
			try:
				track_info = eyed3.load(mp3)
				artist = track_info.tag.artist
				title = track_info.tag.title
				sql = 'INSERT INTO audiotracks (artist, title, path) VALUES ("%s", "%s", "%s")' % (artist, title, mp3)
				self.con.execute(sql)
			except AttributeError:
				pass
		self.db.commit()
		return "done"

	def get_random_tracks(self):
		sql = "SELECT * FROM audiotracks ORDER BY RANDOM() LIMIT %d" % self.config['n_choices']
		self.con.execute(sql)
		result = self.con.fetchall()
		dict_res = []
		for i in result:
			dict_res.append(dict(i))
		return dict_res

	def add_vote(self, voted_val):
		voted_val = int(voted_val)
		sql = "INSERT INTO votes (votefor) VALUES (%d)" % voted_val
		self.con.execute(sql)
		self.db.commit()

	def sum_votes(self):
		res_list = []
		res_dict = {}
		for i in range(self.config['n_choices']):
			res_list.append(0)
		sql = "SELECT votefor FROM votes"
		self.con.execute(sql)
		result = self.con.fetchall()
		res_dict["total_votes"] = len(result)
		for i in result:
			res_list[i['votefor']] += 1
		res_dict["sum"] = res_list
		max_value = max(res_list)
		res_dict["max_value"] = max_value
		j = 0
		max_list = []
		for i in res_list:
			if i == max_value:
				max_list.append(j)
				print("max_list: ", max_list)
			j += 1
		max_index = random.choice(max_list)
		print("win_index: ", max_index)
		res_dict["win_index"] = max_index
		return res_dict

	def empty_box(self):
		sql = "DELETE FROM votes WHERE 1"
		self.con.execute(sql)
		self.db.commit()


class OSInterface:

	def __init__(self):
		pass

	def shutdown(self):
		subprocess.Popen("sudo shutdown -h now", shell=True)


if __name__ == "__main__":

	dbif = DBInterface()
	#print(dbif.get_config())
	print(dbif.scan_files())

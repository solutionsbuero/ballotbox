//===============================
// Fullscreen forcieren
//===============================
$(function() {
    openFullscreen();
});

function openFullscreen() {
  var elem = document.documentElement;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}


//===============================
// SSE-Event anhängen
//===============================
var evtSrc = new EventSource("/subscribe");
evtSrc.onmessage = function(e) {
    console.log(e.data);
    obj_data = JSON.parse(e.data);
    event_switch(obj_data);
};

function event_switch(obj) {
    switch(obj.action) {
        case "playback_started":
            playback_started();
            break;
        case "next_vote":
            next_vote(obj);
            break;
        case "got_vote":
            got_vote(obj["for"]);
            break;
        case "playback_stopped":
            playback_stopped();
            break;
        case "20left":
            got20left();
            break;
        case "show_results":
            show_results(obj.options, obj.results);
    }
}

//===============================
// SSE-Funktionen
//===============================

function playback_started() {
    $("#hinfo_right").text("Playback started");
}

function playback_stopped() {
    $("#hinfo_right").text("Playback stopped");
}

function next_vote(vote_data) {
    $("#hinfo_center").text("Next up");
    $("#current_track").text(vote_data["current_track"])
    for(i = 0; i < vote_data["value"].length; i++) {
        output_text = vote_data["value"][i].artist + "<br />" + vote_data["value"][i].title;
        $("#text_choice" + String(i)).html(output_text);    
    }
    $("#hinfo_left").text("Polls are open");
    $("#current_track_con").slideDown("fast");
    $("#voting_booth").slideDown("fast");
    $("#result_page").slideUp("fast");
}

function got_vote(voted_for) {
    color_array = ['#383276', '#9D344B', '#549431', '#AA9139'];
    $("#choice" + String(voted_for)).css("box-shadow", "0 0 2em " + color_array[parseInt(voted_for)]);
    setTimeout(function () {
        $("#choice" + String(voted_for)).css("box-shadow", "none");
    }, 300);
}

function got20left() {
    $("#hinfo_left").text("Polls are closing in a few seconds");
}

function show_results(options, results) {
    $("#hinfo_center").text("Results");
    $("#hinfo_left").text("Polls are closed");
    for(i = 0; i < options.length; i++) {
        output_text = options[i].artist + "<br />" + options[i].title;
        $("#res_cell_top_" + i).html(output_text);
        if(results.total_votes == 0) {
            abs_percent = 0;
            rel_percent = 0;
        }
        else {
            abs_percent = round((results.sum[i]/results.total_votes)*100, 2);
            rel_percent = round((results.sum[i]/results.max_value)*100, 2);
        }
        output_text = results.sum[i] + "/" + results.total_votes + " votes<br />" + abs_percent + "%";
        $("#res_cell_bottom_" + i).html(output_text);
        $("#res_bar" + i).css('height', rel_percent + "%");
        if(i == results.win_index) {
            $("#elected" + i).show();
        }
        else {
            $("#elected" + i).hide();
        }
    }
    $("#voting_booth").slideUp("fast");
    $("#current_track_con").slideUp("fast");
    $("#result_page").slideDown("fast");
    
}

//===============================
// Menuführung
//===============================

function toogle_menu() {
    if($("#menu").is(":visible")) {
        $("#menu").slideUp();
    }
    else {
        $("#menu").slideDown();
    }
}

function start_playback() {
    $.ajax({
        url: "/start_playback",
        type: "GET"
    });
    toogle_menu();
}

function stop_playback() {
     $.ajax({
        url: "/stop_playback",
        type: "GET"
    });
    $("#hinfo_right").text("Stopping playback after this");
    toogle_menu();
}

function shutdown() {
    $.ajax({
        url: "/shutdown",
        type: "GET"
    });
    $("#hinfo_right").text("Shutting down...");
    toogle_menu();
}

//===============================
// Test-Funktionen
//===============================

function test_vote(choice) {
    $.ajax({
        url: "/cast_vote/" + choice,
        type: "GET"
    });
}


//===============================
// Utilites
//===============================

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}
